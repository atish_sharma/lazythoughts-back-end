package com.shadowofsusanoo.chidori.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author atish
 */
@Controller @RequestMapping("") public class DefaultController {
    
    @RequestMapping("/") public String index(){
        return "redirect:/view/index.html";
    }
    
}
