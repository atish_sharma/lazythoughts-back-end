package com.shadowofsusanoo.chidori.controller;

import com.shadowofsusanoo.chidori.hibernate.Answer;
import com.shadowofsusanoo.chidori.hibernate.GroupMember;
import com.shadowofsusanoo.chidori.hibernate.Option;
import com.shadowofsusanoo.chidori.hibernate.Question;
import com.shadowofsusanoo.chidori.hibernate.User;
import com.shadowofsusanoo.chidori.service.HibernateService;
import com.shadowofsusanoo.chidori.service.HttpSessionService;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Transaction;

/**
 *
 * @author atish
 */
@Controller @RequestMapping("/question") public class QuestionController {
    
    public static final long INVALID_QUESTION_ID = -1;
    
    @Autowired private HttpSession session;
    @Autowired private HibernateService hibernateService;
    private Session hibernateSession;
    private String sessionId;
    
    @RequestMapping("create") public @ResponseBody QuestionModel createQuestion(@RequestBody QuestionModel questionModel){
        sessionId=questionModel.getSessionId();
        hibernateSession=hibernateService.getSession();
        try{
            if(isValidSessionId()){
                insertNewQuestion(questionModel);
            }else{
                questionModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
            questionModel.setSuccess(false);
        }
        sessionId=null;
        hibernateService.closeSession();
        return questionModel;
    }
    
    @RequestMapping("id") public @ResponseBody QuestionModel getQuestionById(@RequestBody QuestionModel questionModel){
        sessionId=questionModel.getSessionId();
        hibernateSession=hibernateService.getSession();
        try{
            if(isValidSessionId()){
                questionModel = getQuestionModel(questionModel.getQuestionId());
            }else{
                questionModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
            questionModel.setSuccess(false);
        }
        sessionId=null;
        hibernateService.closeSession();
        return questionModel;
    }
    
    @RequestMapping("repository") public @ResponseBody ArrayList<QuestionModel> getQuestionRepository(@RequestBody QuestionModel questionModel){
        sessionId=questionModel.getSessionId();
        hibernateSession=hibernateService.getSession();
        ArrayList<QuestionModel> repo=new ArrayList<>();
        try{
            if(isValidSessionId()){
                for(Long id:getQuestionRepository()){
                    repo.add(getQuestionModel(id));
                }
                questionModel.setSuccess(true);
            }else{
                questionModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
            questionModel.setSuccess(false);
        }
        sessionId=null;
        hibernateService.closeSession();
        return repo;
    }
    
    @RequestMapping("id/latest") public @ResponseBody QuestionModel getLatestQuestionId(@RequestBody QuestionModel questionModel){
        sessionId=questionModel.getSessionId();
        hibernateSession=hibernateService.getSession();
        try{
            if(isValidSessionId()){
                questionModel.setQuestionId(getLatestAnswerableQuestionId(questionModel.getCategory()));
                questionModel.setSuccess(true);
            }else{
                questionModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
            questionModel.setSuccess(false);
        }
        sessionId=null;
        hibernateService.closeSession();
        return questionModel;
    }
    
    @RequestMapping("id/previous") public @ResponseBody QuestionModel getPreviousQuestionId(@RequestBody QuestionModel questionModel){
        sessionId=questionModel.getSessionId();
        hibernateSession=hibernateService.getSession();
        try{
            if(isValidSessionId()){
                questionModel.setQuestionId(getPreviousAnswerableQuestionId(questionModel.getQuestionId(),questionModel.getCategory()));
                questionModel.setSuccess(true);
            }else{
                questionModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
            questionModel.setSuccess(false);
        }
        sessionId=null;
        hibernateService.closeSession();
        return questionModel;
    }
    
    @RequestMapping("id/next") public @ResponseBody QuestionModel getNextQuestionId(@RequestBody QuestionModel questionModel){
        sessionId=questionModel.getSessionId();
        hibernateSession=hibernateService.getSession();
        try{
            if(isValidSessionId()){
                questionModel.setQuestionId(getNextAnswerableQuestionId(questionModel.getQuestionId(),questionModel.getCategory()));
                questionModel.setSuccess(true);
            }else{
                questionModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
            questionModel.setSuccess(false);
        }
        sessionId=null;
        hibernateService.closeSession();
        return questionModel;
    }
    
    @RequestMapping("delete") public @ResponseBody QuestionModel deleteQuestion(@RequestBody QuestionModel questionModel){
        sessionId=questionModel.getSessionId();
        hibernateSession=hibernateService.getSession();
        try{
            if(isValidSessionId()){
                deleteQuestion(questionModel.getQuestionId(),hibernateSession);
                questionModel.setSuccess(true);
            }else{
                questionModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
            questionModel.setSuccess(false);
        }
        sessionId=null;
        hibernateService.closeSession();
        return questionModel;
    }
    
    static public void deleteQuestion(long questionId,Session hibernateSession){
        //delete answers
        //delete options 
        //delete group members
        //delete question
        Transaction transaction = hibernateSession.beginTransaction();
        String statement = "DELETE FROM Answer WHERE questionId = :qid";
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        query.executeUpdate();
        statement = "DELETE FROM Option WHERE questionId = :qid";
        query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        query.executeUpdate();
        statement = "DELETE FROM GroupMember WHERE questionId = :qid";
        query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        query.executeUpdate();
        statement = "DELETE FROM Question WHERE id = :qid";
        query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        query.executeUpdate();
        hibernateSession.flush();
        transaction.commit();
    }
    
    private boolean isValidSessionId(){
        return HttpSessionService.find(sessionId)!=null;
    }
    
    private User getUser(){
        return (User)HttpSessionService.find(sessionId).getAttribute(SessionController.SESSION_ATTRIBUTE_USER);
    }
    
    private boolean isAnswerable(long questionId,String category){
        Question question=getQuestion(questionId);
        category=category.trim().toLowerCase();
        if(question==null){
            return false;
        }else{
            if(question.getUserId()==getUser().getId()){
                return false;
            }
            if(!category.equals("all")){
                if(!question.getCategory().equals(category)){
                    return false;
                }
            }
            if(question.isRestricted()){
                String statement = "FROM GroupMember WHERE questionId = :qid AND userId = :uid";
                Query query=hibernateSession.createQuery(statement);
                query.setParameter("uid", getUser().getId());
                query.setParameter("qid", question.getId());
                List list=query.list();
                if(list.size()==0){
                    return false;
                }
            }
        }
        return true;
    }
    
    private Question getQuestion(long questionId){
        String statement = "FROM Question WHERE id = :qid";
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        ArrayList<Question> list=(ArrayList<Question>)query.list();
        if(list.size()==0)
            return null;
        return list.get(0);
    }
    
    private ArrayList<String> getOptions(long questionId){
        String statement = "FROM Option WHERE questionId = :qid ORDER BY optionNumber ASC" ;
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        ArrayList<String> options=new ArrayList<String>();
        for(Option option:(ArrayList<Option>)query.list()){
            options.add(option.getOption());
        }
        return options;
    }
    
    private ArrayList<Long> getGroupMembers(long questionId){
        String statement = "FROM GroupMember WHERE questionId = :qid";
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        ArrayList<Long> members=new ArrayList<Long>();
        for(GroupMember member:(ArrayList<GroupMember>)query.list()){
            members.add(member.getUserId());
        }
        return members;
    }
    
    private HashMap<Integer,Long> getAnswers(long questionId){
        String statement = "FROM Answer WHERE questionId = :qid";
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("qid", questionId);
        ArrayList<Answer> answers=(ArrayList<Answer>)query.list();
        HashMap<Integer,Long> hash=new HashMap<>();
        for(Answer a:answers){
            if(hash.containsKey(a.getOptionNumber())){
                hash.put(a.getOptionNumber(), hash.get(a.getOptionNumber())+1);
            }else{
                hash.put(a.getOptionNumber(),(long)1);
            }
        }
        return hash;
    }
    
    public QuestionModel getQuestionModel(long questionId,Session hibernateSession){
        this.hibernateSession=hibernateSession;
        return getQuestionModel(questionId);
    }
    
    private QuestionModel getQuestionModel(long questionId){
        QuestionModel questionModel=new QuestionModel();
        questionModel.setSuccess(true);
        questionModel.setSessionId(sessionId);
        Question Q=getQuestion(questionId);
        if(Q!=null){
            questionModel.setOptions(getOptions(questionId));
            questionModel.setMembers(getGroupMembers(questionId));
            questionModel.setAnswers(getAnswers(questionId));
            questionModel.setCategory(Q.getCategory());
            questionModel.setQuestion(Q.getQuestion());
            questionModel.setQuestionId(Q.getId());
            questionModel.setUserId(Q.getUserId());
            questionModel.setTimestamp(Q.getTimestamp());
            questionModel.setRestricted(Q.isRestricted());
            questionModel.setMultipleAnswers(Q.isMultipleAnswers());
        }else{
            questionModel.setSuccess(false);
        }
        return questionModel;
    }
    
    private ArrayList<Long> getQuestionRepository(){
        long userId=getUser().getId();
        String statement = "SELECT id FROM Question WHERE userId = :uid";
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("uid", getUser().getId());
        return (ArrayList<Long>)query.list();
    }
    
    private void insertNewQuestion(QuestionModel questionModel){
        Transaction transaction=hibernateSession.beginTransaction();
        Question Q=new Question();
        Q.setQuestion(questionModel.getQuestion().trim());
        Q.setRestricted(questionModel.isRestricted());
        Q.setCategory(questionModel.getCategory().trim().toLowerCase());
        Q.setMultipleAnswers(questionModel.isMultipleAnswers());
        Q.setTimestamp(System.currentTimeMillis());
        Q.setUserId(getUser().getId());
        hibernateSession.save(Q);
        int count=0;
        for(String s:questionModel.getOptions()){
            Option option=new Option();
            option.setOption(s.trim());
            option.setOptionNumber(count++);
            option.setQuestionId(Q.getId());
            hibernateSession.save(option);
        }
        if(Q.isRestricted()){
            for(Long l:questionModel.getMembers()){
                GroupMember groupMember = new GroupMember(); 
                groupMember.setQuestionId(Q.getId());
                groupMember.setUserId(l);
                hibernateSession.save(groupMember);
            }
        }
        hibernateSession.flush();
        transaction.commit();
        questionModel.setTimestamp(Q.getTimestamp());
        questionModel.setUserId(Q.getUserId());
        questionModel.setCategory(Q.getCategory());
        questionModel.setQuestionId(Q.getId());
        questionModel.setQuestion(Q.getQuestion());
        questionModel.setSuccess(true);
    }
    
    private long getPreviousAnswerableQuestionId(long questionId,String category){
        String statement="FROM Question WHERE id < :id ORDER BY id DESC";
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("id",questionId);
        ArrayList<Question> list=(ArrayList<Question>)query.list();
        for(Question Q:list){
            if(isAnswerable(Q.getId(),category)){
                return Q.getId();
            }
        }
        return INVALID_QUESTION_ID;
    }
    
    private long getNextAnswerableQuestionId(long questionId,String category){
        String statement="FROM Question WHERE id > :id ORDER BY id ASC";
        Query query=hibernateSession.createQuery(statement);
        query.setParameter("id",questionId);
        ArrayList<Question> list=(ArrayList<Question>)query.list();
        for(Question Q:list){
            if(isAnswerable(Q.getId(),category)){
                return Q.getId();
            }
        }
        return INVALID_QUESTION_ID;
    }
    
    private long getLatestAnswerableQuestionId(String category){
        String statement="FROM Question ORDER BY id DESC";
        Query query=hibernateSession.createQuery(statement);
        ArrayList<Question> list=(ArrayList<Question>)query.list();
        for(Question Q:list){
            if(isAnswerable(Q.getId(),category)){
                return Q.getId();
            }
        }
        return INVALID_QUESTION_ID;
    }
    
}

class QuestionModel{
    
    private String sessionId;
    private String question;
    private String category;
    private ArrayList<String> options;
    private ArrayList<Long> members;
    private HashMap<Integer,Long> answers;
    private boolean multipleAnswers;
    private boolean success;
    private boolean restricted;
    private long questionId;
    private long userId;
    private long timestamp;
    private ArrayList<Long> questionRepository;

    public String getSessionId() {
        return sessionId;
    }

    public ArrayList<Long> getQuestionRepository() {
        return questionRepository;
    }

    public void setQuestionRepository(ArrayList<Long> questionRepository) {
        this.questionRepository = questionRepository;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public HashMap<Integer, Long> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<Integer, Long> answers) {
        this.answers = answers;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }

    public ArrayList<Long> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Long> members) {
        this.members = members;
    }

    public boolean isMultipleAnswers() {
        return multipleAnswers;
    }

    public void setMultipleAnswers(boolean multipleAnswers) {
        this.multipleAnswers = multipleAnswers;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isRestricted() {
        return restricted;
    }

    public void setRestricted(boolean restricted) {
        this.restricted = restricted;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
    
}
