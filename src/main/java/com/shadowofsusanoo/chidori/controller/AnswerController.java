package com.shadowofsusanoo.chidori.controller;

import com.shadowofsusanoo.chidori.hibernate.Answer;
import com.shadowofsusanoo.chidori.hibernate.User;
import com.shadowofsusanoo.chidori.service.HibernateService;
import com.shadowofsusanoo.chidori.service.HttpSessionService;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author atish
 */
@Controller @RequestMapping("/answer") public class AnswerController {
    
    @Autowired private HttpSession session;
    @Autowired private HibernateService hibernateService;
    private Session hibernateSession;    
    
    @RequestMapping("id") public @ResponseBody Object[] answer (@RequestBody AnswerModel answerModel){
        hibernateSession = hibernateService.getSession();
        Object[] arr={null,null};
        arr[0]=answerModel;
        try{
            if(HttpSessionService.find(answerModel.getSessionId())!=null){
                HttpSession httpSession=HttpSessionService.find(answerModel.getSessionId());
                long id=((User)httpSession.getAttribute(SessionController.SESSION_ATTRIBUTE_USER)).getId();
                Transaction transaction = hibernateSession.beginTransaction();
                String statement = "DELETE FROM Answer WHERE userId = :id AND questionId = :qid";
                Query query=hibernateSession.createQuery(statement);
                query.setParameter("id", id);
                query.setParameter("qid", answerModel.getQuestionId());
                query.executeUpdate();
                for(Integer i:answerModel.getAnswer()){
                    Answer answer=new Answer();
                    answer.setOptionNumber(i);
                    answer.setUserId(id);
                    answer.setQuestionId(answerModel.getQuestionId());
                    hibernateSession.save(answer);
                }
                hibernateSession.flush();
                transaction.commit();
                arr[1]=(new QuestionController()).getQuestionModel(answerModel.getQuestionId(),hibernateSession);
                answerModel.setSuccess(true);
            }else{
                answerModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        hibernateService.closeSession();
        return arr;
    }
    
    @RequestMapping("answered") public @ResponseBody AnswerModel answered (@RequestBody AnswerModel answerModel){
        hibernateSession = hibernateService.getSession();
        try{
            if(HttpSessionService.find(answerModel.getSessionId())!=null){
                HttpSession httpSession=HttpSessionService.find(answerModel.getSessionId());
                long id=((User)httpSession.getAttribute(SessionController.SESSION_ATTRIBUTE_USER)).getId();
                String statement = "FROM Answer WHERE userId = :id AND questionId = :qid";
                Query query=hibernateSession.createQuery(statement);
                query.setParameter("id", id);
                query.setParameter("qid", answerModel.getQuestionId());
                ArrayList<Answer> answers=(ArrayList<Answer>)query.list();
                ArrayList<Integer> options=new ArrayList<>();
                for(Answer a:answers){
                    options.add(a.getOptionNumber());
                }
                answerModel.setAnswer(options);
                answerModel.setSuccess(true);
            }else{
                answerModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
        }
        hibernateService.closeSession();
        return answerModel;
    }
    
}

class AnswerModel{

    private ArrayList<Integer> answer;
    private String sessionId;
    private boolean success;
    private long questionId;

    public ArrayList<Integer> getAnswer() {
        return answer;
    }

    public void setAnswer(ArrayList<Integer> answer) {
        this.answer = answer;
    }
    
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
    
}
