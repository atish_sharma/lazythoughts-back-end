package com.shadowofsusanoo.chidori.controller;

import com.shadowofsusanoo.chidori.hibernate.User;
import com.shadowofsusanoo.chidori.service.HibernateService;
import com.shadowofsusanoo.chidori.service.HttpSessionService;
import java.security.MessageDigest;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author atish
 */
@Controller @RequestMapping("/") public class SessionController {
 
    public static final char[] USERNAME_SPECIAL_CHARS={'.','_'};
    public static final char[] PASSWORD_SPECIAL_CHARS={'.','_','@'};
    public static final String INVALID_SESSION_ID="-1";
    public static final long INVALID_USER_ID=-1;
    public static final String SESSION_ATTRIBUTE_USER="attributeUser";
    public static final int PASSWORD_MIN_LEN=3;
    public static final int PASSWORD_MAX_LEN=30;
    public static final int USERNAME_MIN_LEN=3;
    public static final int USERNAME_MAX_LEN=30;
    
    @Autowired private HttpSession session;
    @Autowired private HibernateService hibernateService;
    private Session hibernateSession;
    
    @RequestMapping("sign/up") public @ResponseBody SessionModel signUp(@RequestBody SessionModel sessionModel){
        hibernateSession = hibernateService.getSession();
        try{
            sessionModel.getPassword().trim();
            sessionModel.getUsername().trim().toLowerCase();
            boolean valid = true;
            valid = valid & sessionModel.getPassword().validate(PASSWORD_MIN_LEN, PASSWORD_MAX_LEN, PASSWORD_SPECIAL_CHARS);
            valid = valid & sessionModel.getUsername().validate(USERNAME_MIN_LEN, USERNAME_MAX_LEN, USERNAME_SPECIAL_CHARS);
            if(valid){
                String statement="FROM User WHERE username = :username";
                Query query=hibernateSession.createQuery(statement);
                query.setParameter("username", sessionModel.getUsername().getValue());
                List list=query.list();
                if(list.size()>0){
                    sessionModel.setUniqueUsername(false);
                    valid=false;
                }
            }
            if(valid){
                User user=new User();
                user.setPassword(hashPassword(sessionModel.getPassword().getValue()));
                user.setUsername(sessionModel.getUsername().getValue());
                user.setDob(sessionModel.getDob().trim());
                user.setSignupTimestamp(System.currentTimeMillis());
                Transaction transaction = hibernateSession.beginTransaction();
                hibernateSession.save(user);
                hibernateSession.flush();
                transaction.commit();
                session.setAttribute(SESSION_ATTRIBUTE_USER, user);
                sessionModel.setUniqueUsername(true);
                sessionModel.setSessionId(session.getId());
                sessionModel.setUserId(user.getId());
                sessionModel.setDob(user.getDob());
                sessionModel.setSignupTimestamp(user.getSignupTimestamp());
            }else{
                sessionModel.setUserId(INVALID_USER_ID);
                sessionModel.setSessionId(INVALID_SESSION_ID);
            }
            sessionModel.getPassword().setValue(null);
        }catch(Exception e){
            //e.printStackTrace();
        }
        hibernateService.closeSession();
        return sessionModel;
    }
    
    @RequestMapping("log/in") public @ResponseBody SessionModel logIn(@RequestBody SessionModel sessionModel){
        hibernateSession = hibernateService.getSession();
        try{
            sessionModel.getPassword().trim();
            sessionModel.getUsername().trim().toLowerCase();
            String statement="FROM User WHERE username = :username AND password = :password";
            Query query=hibernateSession.createQuery(statement);
            query.setParameter("username", sessionModel.getUsername().getValue());
            query.setParameter("password", hashPassword(sessionModel.getPassword().getValue()));
            List list=query.list();
            sessionModel.setPassword(null);
            if(list.size()>0){
                User user=(User)list.get(0);
                sessionModel.getUsername().setValue(user.getUsername());
                sessionModel.setDob(user.getDob());
                sessionModel.setSignupTimestamp(user.getSignupTimestamp());
                sessionModel.setUserId(user.getId());
                sessionModel.setSessionId(session.getId());
                session.setAttribute(SESSION_ATTRIBUTE_USER, user);
            }else{
                sessionModel.setSessionId(INVALID_SESSION_ID);
                sessionModel.setUserId(INVALID_USER_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
        }
        hibernateService.closeSession();
        return sessionModel;
    }
    
    @RequestMapping("log/out") public @ResponseBody SessionModel logOut(@RequestBody SessionModel sessionModel){
        try{
            HttpSession logOutSession=HttpSessionService.find(sessionModel.getSessionId());
            sessionModel.setSessionId(logOutSession.getId());
            sessionModel.setUserId(((User)logOutSession.getAttribute(SESSION_ATTRIBUTE_USER)).getId());
            logOutSession.removeAttribute(SESSION_ATTRIBUTE_USER);
            logOutSession.invalidate();
            logOutSession=null;
        }catch(Exception e){
            //e.printStackTrace();
            sessionModel.setSessionId(INVALID_SESSION_ID);
            sessionModel.setUserId(INVALID_USER_ID);
        }
        return sessionModel;
    }
    
    public static String hashPassword(String password){
        String salt1="123ghtt";
        String salt2="90..//-";
        password=salt1+password+salt2;
        try{
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        }catch(Exception e){
            //e.printStackTrace();
        }
        return password;
    }
    
}

class SessionModel {
    
    private SessionParameter username;
    private SessionParameter password;
    private long userId;
    private String sessionId;
    private boolean uniqueUsername;
    private String dob;
    private long signupTimestamp;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public long getSignupTimestamp() {
        return signupTimestamp;
    }

    public void setSignupTimestamp(long signupTimestamp) {
        this.signupTimestamp = signupTimestamp;
    }

    public boolean isUniqueUsername() {
        return uniqueUsername;
    }

    public void setUniqueUsername(boolean uniqueUsername) {
        this.uniqueUsername = uniqueUsername;
    }

    public SessionParameter getUsername() {
        return username;
    }

    public void setUsername(SessionParameter username) {
        this.username = username;
    }

    public SessionParameter getPassword() {
        return password;
    }

    public void setPassword(SessionParameter password) {
        this.password = password;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
}

class SessionParameter{
    
    private boolean tooShort;
    private boolean tooLong;
    private boolean invalidCharacters;
    private boolean valid;
    private String value;

    public SessionParameter trim(){
        value=value.trim();
        return this;
    }
    
    public SessionParameter toLowerCase(){
        value=value.toLowerCase();
        return this;
    }
    
    public boolean isTooShort() {
        return tooShort;
    }

    public void setTooShort(boolean tooShort) {
        this.tooShort = tooShort;
    }

    public boolean isTooLong() {
        return tooLong;
    }

    public void setTooLong(boolean tooLong) {
        this.tooLong = tooLong;
    }

    public boolean isInvalidCharacters() {
        return invalidCharacters;
    }

    public void setInvalidCharacters(boolean invalidCharacters) {
        this.invalidCharacters = invalidCharacters;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public boolean validate(int minLength,int maxLength,char[] validSpecialCharacters){
        int n=value.length();
        valid=true;
        if(n<minLength){
            tooShort=true;
            valid=false;
        }else{
            tooShort=false;
        }
        if(n>maxLength){
            tooLong=true;
            valid=false;
        }else{
            tooLong=false;
        }
        invalidCharacters=false;
        if(valid){
            for(int i=0;i<n;i++){
                char c=value.charAt(i);
                if(Character.isAlphabetic(c) || Character.isDigit(c))
                       continue;
                valid=false;
                for(Character ch:validSpecialCharacters){
                    if(ch==c){
                        valid=true;
                        break;
                    }
                }
                if(!valid){
                    invalidCharacters=true;
                    break;
                }
            }
        }
        return valid;
    }
    
}
