package com.shadowofsusanoo.chidori.controller;

import com.shadowofsusanoo.chidori.hibernate.User;
import com.shadowofsusanoo.chidori.service.HibernateService;
import com.shadowofsusanoo.chidori.service.HttpSessionService;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.ArrayList;

/**
 *
 * @author atish
 */
@Controller @RequestMapping("/profile") public class ProfileController {
    
    @Autowired private HttpSession session;
    @Autowired private HibernateService hibernateService;
    private Session hibernateSession;
    
    @RequestMapping("id") public @ResponseBody ProfileModel getProfileById(@RequestBody ProfileModel profileModel){
        hibernateSession = hibernateService.getSession();
        try{
            if(HttpSessionService.find(profileModel.getSessionId())!=null){
                String statement="FROM User WHERE id = :id";
                Query query=hibernateSession.createQuery(statement);
                query.setParameter("id", profileModel.getUserId());
                List list=query.list();
                if(list.size()>0){
                    User user=(User)list.get(0);
                    profileModel.setUserId(user.getId());
                    profileModel.setUsername(user.getUsername());
                    profileModel.setSuccess(true);
                }else{
                    profileModel.setSuccess(false);
                }
            }else{
                profileModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
        }
        hibernateService.closeSession();
        return profileModel;
    }
    
    @RequestMapping("username") public @ResponseBody ProfileModel getProfileByUsername(@RequestBody ProfileModel profileModel){
        hibernateSession = hibernateService.getSession();
        try{
            if(HttpSessionService.find(profileModel.getSessionId())!=null){
                String statement="FROM User WHERE username = :username";
                Query query=hibernateSession.createQuery(statement);
                query.setParameter("username", profileModel.getUsername().trim().toLowerCase());
                List list=query.list();
                if(list.size()>0){
                    User user=(User)list.get(0);
                    profileModel.setUserId(user.getId());
                    profileModel.setUsername(user.getUsername());
                    profileModel.setSuccess(true);
                }else{
                    profileModel.setSuccess(false);
                }
            }else{
                profileModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
        }
        hibernateService.closeSession();
        return profileModel;
    }
    
    @RequestMapping("change/username") @ResponseBody ProfileModel changeUsername(@RequestBody ProfileModel profileModel){
        hibernateSession = hibernateService.getSession();
        try{
            if(HttpSessionService.find(profileModel.getSessionId())!=null){
                profileModel.getNewUsername().trim().toLowerCase();
                boolean valid = true;
                valid = valid & profileModel.getNewUsername().validate(SessionController.USERNAME_MIN_LEN, SessionController.USERNAME_MAX_LEN, SessionController.USERNAME_SPECIAL_CHARS);
                if(valid){
                    long userId=((User)HttpSessionService.find(profileModel.getSessionId()).getAttribute(SessionController.SESSION_ATTRIBUTE_USER)).getId();
                    profileModel.setUserId(userId);
                    String statement="FROM User WHERE username = :username";
                    Query query=hibernateSession.createQuery(statement);
                    query.setParameter("username", profileModel.getNewUsername().getValue());
                    List list=query.list();
                    if(list.size()>0){
                        profileModel.setUniqueUsername(false);
                    }else{
                        profileModel.setUniqueUsername(true);
                    }
                    valid = valid & profileModel.isUniqueUsername();
                    if(valid){
                        statement="UPDATE User SET username = :username WHERE id = :id";
                        Transaction transaction = hibernateSession.beginTransaction();
                        query=hibernateSession.createQuery(statement);
                        query.setParameter("username", profileModel.getNewUsername().getValue());
                        query.setParameter("id", userId);
                        query.executeUpdate();
                        hibernateSession.flush();
                        transaction.commit();
                        profileModel.setSuccess(true);
                        profileModel.setUsername(profileModel.getNewUsername().getValue());
                        profileModel.setNewUsername(null);
                    }
                }
            }else{
                profileModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
        }
        hibernateService.closeSession();
        return profileModel;
    }
    
    @RequestMapping("change/password") @ResponseBody ProfileModel changePassword(@RequestBody ProfileModel profileModel){
        hibernateSession = hibernateService.getSession();
        try{
            if(HttpSessionService.find(profileModel.getSessionId())!=null){
                profileModel.getNewPassword().trim();
                boolean valid = true;
                valid = valid & profileModel.getNewPassword().validate(SessionController.PASSWORD_MIN_LEN, SessionController.PASSWORD_MAX_LEN, SessionController.PASSWORD_SPECIAL_CHARS);
                if(valid){
                    long userId=((User)HttpSessionService.find(profileModel.getSessionId()).getAttribute(SessionController.SESSION_ATTRIBUTE_USER)).getId();
                    profileModel.setUserId(userId);
                    String statement="FROM User WHERE password = :password AND id = :id";
                    Query query=hibernateSession.createQuery(statement);
                    query.setParameter("password", SessionController.hashPassword(profileModel.getOldPassword().trim()));
                    query.setParameter("id", userId);
                    List list=query.list();
                    if(list.size()==0){
                        profileModel.setWrongPassword(true);
                    }else{
                        profileModel.setWrongPassword(false);
                    }
                    valid = valid & !profileModel.isWrongPassword();
                    if(valid){
                        statement="UPDATE User SET password = :password WHERE id = :id";
                        Transaction transaction = hibernateSession.beginTransaction();
                        query=hibernateSession.createQuery(statement);
                        query.setParameter("password", SessionController.hashPassword(profileModel.getNewPassword().getValue()));
                        query.setParameter("id", userId);
                        query.executeUpdate();
                        hibernateSession.flush();
                        transaction.commit();
                        profileModel.setSuccess(true);
                        profileModel.setNewPassword(null);
                    }
                }
            }else{
                profileModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
        }
        profileModel.setOldPassword(null);
        if(profileModel.getNewPassword()!=null)
            profileModel.getNewPassword().setValue(null);
        hibernateService.closeSession();
        return profileModel;
    }
    
    @RequestMapping("delete") @ResponseBody ProfileModel deleteUser(@RequestBody ProfileModel profileModel){
        hibernateSession = hibernateService.getSession();
        try{
            if(HttpSessionService.find(profileModel.getSessionId())!=null){
                //delete questions [options + answers + group members + question]
                //delete answers
                //delete groups
                //delete user
                //log user out
                HttpSession httpSession=HttpSessionService.find(profileModel.getSessionId());
                long id=((User)httpSession.getAttribute(SessionController.SESSION_ATTRIBUTE_USER)).getId();
                String statement = "SELECT id FROM Question WHERE userId = :id";
                Query query=hibernateSession.createQuery(statement);
                query.setParameter("id", id);
                ArrayList<Long> questions=(ArrayList<Long>)query.list();
                for(Long l:questions){
                    QuestionController.deleteQuestion(l, hibernateSession);
                }                
                Transaction transaction = hibernateSession.beginTransaction();
                statement = "DELETE FROM Answer WHERE userId = :id";
                query=hibernateSession.createQuery(statement);
                query.setParameter("id", id);
                query.executeUpdate();
                statement = "DELETE FROM GroupMember WHERE userId = :id";
                query=hibernateSession.createQuery(statement);
                query.setParameter("id", id);
                query.executeUpdate();
                statement = "DELETE FROM User WHERE id = :id";
                query=hibernateSession.createQuery(statement);
                query.setParameter("id", id);
                query.executeUpdate();
                hibernateSession.flush();
                transaction.commit();
                httpSession.setAttribute(SessionController.SESSION_ATTRIBUTE_USER, null);
                httpSession.invalidate();
                httpSession=null;
                profileModel.setSuccess(true);
            }else{
                profileModel.setSessionId(SessionController.INVALID_SESSION_ID);
            }
        }catch(Exception e){
            //e.printStackTrace();
        }
        hibernateService.closeSession();
        return profileModel;
    }
}

class ProfileModel{

    private String sessionId;
    private long userId;
    private String username;
    private String oldPassword;
    private SessionParameter newPassword;
    private SessionParameter newUsername;
    private boolean success;
    private boolean uniqueUsername;
    private boolean wrongPassword;
    
    public boolean isWrongPassword() {
        return wrongPassword;
    }

    public void setWrongPassword(boolean wrongPassword) {
        this.wrongPassword = wrongPassword;
    }

    public boolean isUniqueUsername() {
        return uniqueUsername;
    }

    public void setUniqueUsername(boolean uniqueUsername) {
        this.uniqueUsername = uniqueUsername;
    }

    public SessionParameter getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(SessionParameter newPassword) {
        this.newPassword = newPassword;
    }

    public SessionParameter getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(SessionParameter newUsername) {
        this.newUsername = newUsername;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
