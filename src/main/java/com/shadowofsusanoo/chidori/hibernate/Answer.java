package com.shadowofsusanoo.chidori.hibernate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author atish
 */
@Entity @Table(name="answers") public class Answer implements Serializable{
    
    @Id @GeneratedValue @Column(name="id")private long id;
    @Column(name="user_id")private long userId;
    @Column(name="question_id")private long questionId;
    @Column(name="option_number")private int optionNumber;
    
    public Answer(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }
    
}
