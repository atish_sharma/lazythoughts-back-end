package com.shadowofsusanoo.chidori.hibernate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author atish
 */
@Entity @Table(name="options") public class Option implements Serializable{
    
    @Id @GeneratedValue @Column(name="id")private long id;
    @Column(name="option_statement")private String option;
    @Column(name="question_id")private long questionId;
    @Column(name="option_number")private int optionNumber;
    
    public Option(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public int getOptionNumber() {
        return optionNumber;
    }

    public void setOptionNumber(int optionNumber) {
        this.optionNumber = optionNumber;
    }
    
}
