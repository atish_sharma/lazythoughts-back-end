package com.shadowofsusanoo.chidori.hibernate;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author atish
 */
@Entity @Table(name="users") public class User implements Serializable{
    
    @Id @GeneratedValue @Column(name="id")private long id;
    @Column(name="username")private String username;
    @Column(name="password")private String password;
    @Column(name="dob")private String dob;
    @Column(name="signup_timestamp")private long signupTimestamp;
    
    public User(){}

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public long getSignupTimestamp() {
        return signupTimestamp;
    }

    public void setSignupTimestamp(long signupTimestamp) {
        this.signupTimestamp = signupTimestamp;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
