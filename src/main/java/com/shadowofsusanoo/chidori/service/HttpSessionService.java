package com.shadowofsusanoo.chidori.service;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSession;
import java.util.HashMap;

/**
 *
 * @author atish
 */
public class HttpSessionService implements HttpSessionListener {

    private static final HashMap<String,HttpSession> sessions = new HashMap<>();
    
    @Override
    public void sessionCreated(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        sessions.put(session.getId(), session);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        sessions.remove(event.getSession().getId());
    }
    
    public static HttpSession find(String sessionId) {
        try{
            return sessions.get(sessionId);
        }catch(Exception e){
            //e.printStackTrace();
        }
        return null;
    }
    
}
