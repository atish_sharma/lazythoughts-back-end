package com.shadowofsusanoo.chidori.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author atish
 */
public class HibernateService {
    
    private Session session;
    private SessionFactory sessionFactory;
    
    public Session getSession() {
        closeSession();//just in case a session is already open 
        createSession();
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
    
    public void createSession(){
        AnnotationConfiguration cfg = new AnnotationConfiguration();
        cfg.configure();
        sessionFactory = cfg.buildSessionFactory();
        session = sessionFactory.openSession();
    }
    
    public void closeSession(){
        try{
            session.close();
        }catch(Exception e){
            //e.printStackTrace();
        }
        try{
            sessionFactory.close();
        }catch(Exception e){
            //e.printStackTrace();
        }
        session = null;
    }
    
}
