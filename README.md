# LazyThoughts (formerly known as Chidori) Back End #

# Purpose #

The software will provide the user with a platform to conduct surveys in a simple and efficient manner. Using this software, the user can generate interactive questionnaires aimed at a target audience, analyze the responses to posted questions, and answer questions asked by other users. Results will be visualized both graphically and textually. 

# Project Scope #

The system focuses on only one class of user, interacting with the system directly. Through the system, the various users will be able to conduct surveys in real time, from other users in the system. This makes the product very flexible in its scope, and can be applied to any field where a survey needs to be conducted. Some examples include marketing research, academic surveys, and general knowledge. 

# Operating Environment #

1. Latest web browser
2. Working internet connection

[Visit Web App Repository](https://atish_sharma@bitbucket.org/atish_sharma/lazythoughts-web-app.git)